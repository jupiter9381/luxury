const router = require('express').Router();
const mongojs = require('mongojs');

var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://localhost:27017/";

const db = mongojs('mongodb://localhost:27017/properties', ['details', 'images','facilities']);

//Get all tasks
router.get('/properties', (req, res, next) => {
    db.details.find((err, properties) => {
        if(err) return next(err)
        res.json(properties);
    });
});

router.get('/properties/:type', (req, res, next) => {
    db.details.find({ad_type: req.params.type}, (err, properties) => {
        if(err) return next(err)
        res.json(properties);
    });
});

router.get('/properties/:type/:category', (req, res, next) => {
    db.details.find({ad_type: req.params.type, unit_type: req.params.category}, (err, properties) => {
        if(err) return next(err)
        res.json(properties);
    });
});

router.get('/property/:id', (req, res, next) => {
    db.details.find({unit_reference_no: req.params.id}, (err, property)=>{
        if(err) return next(err);
        res.json(property);
    });
});

router.get('/images/:id', (req, res, next) => {
    db.images.find({property_ref_no: req.params.id}, (err, images)=>{
        if(err) return next(err);
        res.json(images);
    });
});

router.get('/facilities/:id', (req, res, next) => {
    db.facilities.find({property_ref_no: req.params.id}, (err, facilities)=>{
        if(err) return next(err);
        res.json(facilities);
    });
});

module.exports = router;