const router = require('express').Router();

router.get('/', (req, res, next) => {
	//res.render("index.html");
    var fs = require('fs'),
        xml2js = require('xml2js');
    var parser = new xml2js.Parser();

    fs.readFile(__dirname +'/index.xml', function(err, data){
        if (!err) {
            parser.parseString(data, function (err, result) {
                
                var listings = result['Listings']['Listing'];

                var MongoClient = require('mongodb').MongoClient;
                var url = "mongodb://localhost:27017/";

                MongoClient.connect(url, function(err, db){
                    if(err) throw err;
                    var dbo = db.db('properties');
                    for(var i = 0; i < listings.length; i++){
                        var count = listings[i]['count'][0];
                        var ad_type = listings[i]['Ad_Type'][0];
                        var unit_type = listings[i]['Unit_Type'][0];
                        var unit_model = listings[i]['Unit_Model'][0];
                        var primary_view = listings[i]['Primary_View'][0];
                        var unit_builtup_area = listings[i]['Unit_Builtup_Area'][0];
                        var unit_model = listings[i]['Unit_Model'][0];
                        var no_of_bathroom = listings[i]['No_of_Bathroom'][0];
                        var property_title = listings[i]['Property_Title'][0];
                        var web_remarks = listings[i]['Web_Remarks'][0];
                        var emirate = listings[i]['Emirate'][0];
                        var community = listings[i]['Community'][0];
                        var exclusive = listings[i]['Exclusive'][0];
                        var cheques = listings[i]['Cheques'][0];
                        var plot_area = listings[i]['Plot_Area'][0];
                        var property_name = listings[i]['Property_Name'][0];
                        var property_ref_no = listings[i]['Property_Ref_No'][0];
                        var listing_agent = listings[i]['Listing_Agent'][0];
                        var listing_agent_phone = listings[i]['Listing_Agent_Phone'][0];
                        var listing_date = listings[i]['Listing_Date'][0];
                        var last_updated = listings[i]['Last_Updated'][0];
                        var bedrooms = listings[i]['Bedrooms'][0];
                        var listing_agent_email = listings[i]['Listing_Agent_Email'][0];
                        var price = listings[i]['Price'][0];
                        var unit_reference_no = listings[i]['Unit_Reference_No'][0];
                        var no_of_rooms = listings[i]['No_of_Rooms'][0];
                        var latitude = listings[i]['Latitude'][0];
                        var longitude = listings[i]['Longitude'][0];
                        var unit_measure = listings[i]['unit_measure'][0];
                        var featured = listings[i]['Featured'][0];

                        var company_name = listings[i]['company_name'][0];
                        var price_on_application = listings[i]['price_on_application'][0];
                        var off_plan = listings[i]['off_plan'][0];
                        var permit_number = listings[i]['permit_number'][0];
                        var completion_status = listings[i]['completion_status'][0];

                        var detail = {count: count, ad_type: ad_type, unit_type: unit_type, unit_model: unit_model,
                            primary_view: primary_view, unit_builtup_area: unit_builtup_area, no_of_bathroom: no_of_bathroom,
                            property_title: property_title, web_remarks: web_remarks, emirate: emirate, community: community,
                            exclusive: exclusive, cheques: cheques, plot_area: plot_area, property_name: property_name,
                            property_ref_no: property_ref_no, listing_agent: listing_agent, listing_agent_phone: listing_agent_phone,
                            listing_date: listing_date, last_updated: last_updated, bedrooms: bedrooms, listing_agent_email: listing_agent_email,
                            price: price, unit_reference_no: unit_reference_no, no_of_rooms: no_of_rooms, latitude: latitude,
                            longitude: longitude, unit_measure: unit_measure, featured: featured, price_on_application: price_on_application,
                            off_plan: off_plan, permit_number: permit_number, completion_status: completion_status 
                        }

                        dbo.collection("details").insertOne(detail, function(err, res){
                            if(err) throw err;
                        }); 
                    }
                    db.close();
                });
                
            });
        } else {
            console.log("err");
        }
    });
    
});
module.exports = router;