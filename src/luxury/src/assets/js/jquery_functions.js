$(document).ready(function(){

//Click event to top search bar
	$('.search-icon').click(function() {
		  $('.search-container').delay(100).slideToggle(500);
		  $('.top_search').delay(100).slideToggle(500);
		  $('.drag').delay(100).slideToggle(500);
	});
	
	
	
	$('.dropdown').hover(
        function() {
            $(this).find('.dropdown-menu').stop(true, true).delay(100).fadeIn();
        }, 
        function() {
            $(this).find('.dropdown-menu').stop(true, true).delay(100).fadeOut();
        }
    );

    $('.dropdown-menu').hover(
        function() {
            $(this).stop(true, true);
        },
        function() {
            $(this).stop(true, true).delay(100).fadeOut();
        }
    );
	
	
	//banners in list details page------------------------------------------------//
	var windowHeight = $(window).height();
	var detailsBannerHeight = windowHeight - ( windowHeight*20/100 );
	var galleryBannerHeight = windowHeight - ( windowHeight*75/100 );
	//alert ('window-'+windowHeight);
	//alert ('window half-'+windowHeightNew);
	$('.lux-property-banner').css("height", detailsBannerHeight+"px")
	
	$('.lux-property-tabs .nav-pills li:nth-child(1), .lux-property-tabs .nav-pills li.dt').click(function(){
		$('.lux-property-banner').css("height", detailsBannerHeight+"px")
	});
	$('.lux-property-tabs .nav-pills li:nth-child(2), .lux-property-tabs .nav-pills li.gl').click(function(){
		$('.lux-property-banner').css("height", galleryBannerHeight+"px");
	});
	//-------------------------------------------------------------------------------//
	
	
	
	$('ul.imageGallery li:nth-child(1)').addClass('col-sm-6');
	$('ul.imageGallery li:nth-child(2)').addClass('col-sm-6'); 
	$('ul.imageGallery li:nth-child(3)').addClass('col-sm-4');
	$('ul.imageGallery li:nth-child(4)').addClass('col-sm-4'); 
	$('ul.imageGallery li:nth-child(5)').addClass('col-sm-4');
	$('ul.imageGallery li:nth-child(6)').addClass('col-sm-6'); 
	$('ul.imageGallery li:nth-child(7)').addClass('col-sm-6');
	$('ul.imageGallery li:nth-child(8)').addClass('col-sm-4'); 
	$('ul.imageGallery li:nth-child(9)').addClass('col-sm-4');
	$('ul.imageGallery li:nth-child(10)').addClass('col-sm-4'); 
	$('ul.imageGallery li:nth-child(11)').addClass('col-sm-6'); 
	$('ul.imageGallery li:nth-child(12)').addClass('col-sm-6');
	$('ul.imageGallery li:nth-child(13)').addClass('col-sm-4'); 
	$('ul.imageGallery li:nth-child(14)').addClass('col-sm-4');
	$('ul.imageGallery li:nth-child(15)').addClass('col-sm-4'); 
	
	// delegate calls to data-toggle="lightbox"
	$(document).delegate('*[data-toggle="lightbox"]', 'click', function(event) {
		event.preventDefault();
		// return $(this).ekkoLightbox({
		// 	always_show_close: true
		// });
	});

	
});

