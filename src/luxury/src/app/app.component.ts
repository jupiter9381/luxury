import { Component, OnInit,  Directive, Renderer, ElementRef} from '@angular/core';

import * as $ from 'jquery';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  constructor(private renderer: Renderer){

  }
  ngOnInit(){
   
  }
  ngAfterViewInit() {
    $(document).ready(function(){
      
    });
    //this.input.nativeElement.focus();
  }
}
