import { Component, OnInit } from '@angular/core';
import "../../../assets/js/jquery.fullpage.js";

declare var $:any;

declare global{
  interface jquery{
    fullpage: JQuery;
  }
}

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],

})
export class HomeComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  ngAfterViewInit(){
    $(document).ready(function(){

      $('#fullpage').fullpage();

      $('#nextp').click(function(e){
				e.preventDefault();
				$.fn.fullpage.moveSectionDown();
      });
    });
  }
}
