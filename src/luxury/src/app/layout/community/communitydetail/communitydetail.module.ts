import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CommunitydetailRoutingModule } from './communitydetail-routing.module';
import { CommunitydetailComponent } from './communitydetail.component';

@NgModule({
  imports: [
    CommonModule,
    CommunitydetailRoutingModule
  ],
  declarations: [
    CommunitydetailComponent,
  ]
})
export class CommunitydetailModule { }
