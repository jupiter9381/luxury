import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CommunitydetailComponent } from './communitydetail.component';

const routes: Routes = [
    {
        path: '', component: CommunitydetailComponent, 
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class CommunitydetailRoutingModule {
}
