import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgbCarouselModule, NgbAlertModule } from '@ng-bootstrap/ng-bootstrap';

import { DetailRoutingModule } from './detail-routing.module';
import { DetailComponent } from './detail.component';
import { EnquireComponent } from '../components/enquire/enquire.component';
import { ShareComponent } from '../components/share/share.component';

import { PropertyService } from '../services/property/property.service';
//import { PricePipeModule } from '../../filters/price/price.module';

@NgModule({
    imports: [
        CommonModule,
        HttpClientModule,
        NgbCarouselModule.forRoot(),
        NgbAlertModule.forRoot(),
        DetailRoutingModule
        //PricePipeModule
    ],
    declarations: [
        DetailComponent,
        EnquireComponent,
        ShareComponent,
    ],
    providers: [PropertyService],
    bootstrap: [DetailComponent]
})
export class DetailModule {}
