import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { Property } from '../list/Property';
import { PropertyService } from '../services/property/property.service';


@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.css']
})
export class DetailComponent implements OnInit {

  reference_id: string;
  property: Property;
  latitude: number;
  longitude: number;

  constructor(private route: ActivatedRoute, private propertyService: PropertyService) { }

  ngOnInit() {
    console.log("detail");
    this.reference_id = this.route.snapshot.params.id;
    
    this.getPropertyById(this.reference_id);

    $(document).ready(function(){
      var windowHeight = $(window).height();
      var detailsBannerHeight = windowHeight - ( windowHeight*20/100 );
      var galleryBannerHeight = windowHeight - ( windowHeight*75/100 );

      $('.lux-property-banner').css("height", detailsBannerHeight+"px")
	
      $('.lux-property-tabs .nav-pills li:nth-child(1), .lux-property-tabs .nav-pills li.dt').click(function(){
        $('.lux-property-banner').css("height", detailsBannerHeight+"px")
      });
      $('.lux-property-tabs .nav-pills li:nth-child(2), .lux-property-tabs .nav-pills li.gl').click(function(){
        $('.lux-property-banner').css("height", galleryBannerHeight+"px");
      });

      $(document).delegate('*[data-toggle="lightbox"]', 'click', function(event) {
        // return $(this).ekkoLightbox({
        // 	always_show_close: true
        // });
      });
    });
  }

  getPropertyById(reference_id){
    this.propertyService.getPropertyById(reference_id)
      .subscribe(property => {
        this.property = property[0];
        this.latitude = property[0].latitude;
        this.longitude = property[0].longitude;
      })
  }

  ngAfterViewChecked(){
    
  }

  
}
