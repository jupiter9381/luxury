import { Component, OnInit, ViewChild, ElementRef, Input } from '@angular/core';
import * as jsPDF from 'jspdf';


@Component({
  selector: 'app-ebrochure',
  templateUrl: './ebrochure.component.html',
  styleUrls: ['./ebrochure.component.css']
})
export class EbrochureComponent implements OnInit {

  @ViewChild('ebrochure') content: ElementRef;
  @Input() images: string[];
  
  constructor() { }

  ngOnInit() {
  }

  downloadPDF(){
    // let doc = new jsPDF();

    // let specialElementHandlers = {
    //   '#editor': function(element, renderer){
    //     return true;
    //   }
    // };

    // let content = this.content.nativeElement;

    // doc.fromHTML(content.innerHTML, 15, 15, {
    //   'width': 1200,
    //   'elementHandlers': specialElementHandlers
    // });

    // doc.save('test.pdf');

    return xepOnline.Formatter.Format('ebrochure', {pageWidth: '216mm', pageHeight: '279mm' ,render: 'download'})
  }
}
