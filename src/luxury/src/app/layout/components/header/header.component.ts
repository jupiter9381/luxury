import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

import * as $ from 'jquery';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }


  /* Search Properties */

  ngAfterViewInit(){
    $(document).ready(function(){

      $('.dropdown').hover(
        function() {
          $(this).find('.dropdown-menu').stop(true, true).delay(100).fadeIn();
        }, 
        function() {
          $(this).find('.dropdown-menu').stop(true, true).delay(100).fadeOut();
        }
      );

      $('.dropdown-menu').hover(
        function() {
          $(this).stop(true, true);
        },
        function() {
          $(this).stop(true, true).delay(100).fadeOut();
        }
      );
    });
  }

  doSearch(){
    var property_ref_no = $("#search_ref").val();
    console.log(property_ref_no);
    //window.location.href=`/search/${property_ref_no}`;
    // this.router.navigate(['/search']);
    // console.log("dfdf");
  }
}
