import { Component, OnInit, Input, Pipe, PipeTransform, AfterViewInit,ViewChild } from '@angular/core';
import { Property } from '../../list/Property';

declare var $:any;

@Component({
  selector: 'app-property',
  templateUrl: './property.component.html',
  styleUrls: ['./property.component.css']
})
export class PropertyComponent implements OnInit, AfterViewInit{

  @Input() property: Property;
  @ViewChild('enquire') enquire;
  constructor() { }
  
  ngOnInit() {
  }

  ngAfterViewInit(){
    
    /* Showing the enquire form when click button */
    $(this.enquire.nativeElement).click(function(e){
      e.preventDefault();
      var name = $(".hd_agent_name").val();
      var phone = $(".hd_agent_phone").val();
      var email = $(".hd_agent_email").val();

      var phone_temp = phone.substr(1, phone.length - 1);
      phone =  "+971 " + phone_temp.substr(0, 2) + " " + phone_temp.substr(2, 3) + " " + phone_temp.substr(5, 4);
      
      /* Set image path */
      var image_path = "assets/images/agent/"+ name.replace(" ", "-") + ".jpg";
      $(".agent_avatar").attr("src", image_path);
      
      /* Set Agent Info*/
      $(".agent-name").html(name);
      $(".agent-number").html(phone);
      $(".agent-email").html(email);
    });
  }

}
