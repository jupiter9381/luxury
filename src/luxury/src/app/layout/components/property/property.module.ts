import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgbCarouselModule, NgbAlertModule } from '@ng-bootstrap/ng-bootstrap';

import { PropertyComponent } from './property.component';


@NgModule({
    imports: [
        CommonModule,
        HttpClientModule,
        NgbCarouselModule.forRoot(),
        NgbAlertModule.forRoot(),
    ],
    declarations: [
        PropertyComponent,
    ],
    providers: [],
    bootstrap: [PropertyComponent]
})
export class PropertyModule {}
