import { Component, OnInit, Input, AfterViewInit, ViewChild} from '@angular/core';
import { Property } from '../../list/Property';

@Component({
  selector: 'app-enquire',
  templateUrl: './enquire.component.html',
  styleUrls: ['./enquire.component.css']
})
export class EnquireComponent implements OnInit {

  @Input() property: Property;
  @ViewChild('title') title;
  constructor() { }

  ngOnInit() {
  }

  ngAfterViewInit(){
    
  }
}
