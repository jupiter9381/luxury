import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LayoutRoutingModule } from './layout-routing.module';  
import { LayoutComponent } from './layout.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';

import {DetailModule} from './detail/detail.module';

import {PricePipeModule} from '../filters/price/price.module';
import { SearchComponent } from './components/search/search.component';


@NgModule({
  imports: [
    CommonModule,
    LayoutRoutingModule,
    PricePipeModule
  ],
  declarations: [
    LayoutComponent, 
    HeaderComponent, 
    FooterComponent,
  ]
})
export class LayoutModule { }
