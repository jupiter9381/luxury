import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import "rxjs/add/operator/map";

import {Property} from '../../list/Property';

@Injectable()
export class PropertyService {

  domain: string = "http://localhost:3000";

  constructor(private http: HttpClient) { }

  getProperties(){
    return this.http.get<Property[]>(`${this.domain}/api/properties`)
      .map(res => res);
  }
  getPropertiesByType(type){
    return this.http.get<Property[]>(`${this.domain}/api/properties/${type}`)
      .map(res => res);
  }
  getPropertiesByTypeAndCategory(type, category){
    return this.http.get<Property[]>(`${this.domain}/api/properties/${type}/${category}`)
      .map(res => res);
  }

  getPropertyById(reference_id){
    return this.http.get<Property>(`${this.domain}/api/property/${reference_id}`)
      .map(res => res);
  }

  getPropertyImagesById(id){
    return this.http.get<string[]>(`${this.domain}/api/images/${id}`)
      .map(res => res);

  }

  getFacilitiesById(id){
    return this.http.get<string[]>(`${this.domain}/api/facilities/${id}`)
      .map(res => res);

  }
}
