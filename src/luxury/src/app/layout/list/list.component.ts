import { Component, OnInit, ElementRef, Renderer2 } from '@angular/core';
import { Property } from './Property';
import { PropertyService } from '../services/property/property.service';
import { ActivatedRoute } from '@angular/router';

import { DomSanitizer } from '@angular/platform-browser';

/* Google Map*/
import  { ViewChild } from '@angular/core';
import { } from '@types/googlemaps';
import { MarkerManager } from '@agm/core';

declare var $:any;

declare global{
  interface jquery{
    ekkoLightbox: JQuery;
  }
}

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

  allProperties: Property[];
  property: Property;
  properties: Property[];
  exclusives: Property[];
  latitude: number;
  longitude: number;
  count: string;
  showFlag: boolean;
  description: string;

  images: string[];
  facilities_array: string[];
  bannerImage: any;
  depth: number;


  facilities: string;
  other_rooms: string;

  @ViewChild('enquire') a:ElementRef;

  constructor(private route: ActivatedRoute, private propertyService: PropertyService, private sanitizer: DomSanitizer, private rd: Renderer2) {
    this.depth = 1;
  };

  ngOnInit() {
    var type = this.route.snapshot.params.type;
    console.log(type);
    var id = this.route.snapshot.params.id;
    if(type == "search")
      this.getProperties();
    if(id == undefined){
      this.showFlag = true;
      var route = type.split("-");
      if(route.length == 1) this.getPropertiesByType(type);
      if(route.length == 3){
        this.getPropertiesByTypeAndCategory(route[2], route[0]);
      }
    } else {
      this.showFlag = false;
      this.getPropertyById(id);
      this.getImagePath(id);
      this.getFacilitiesById(id);
    }

    $(document).ready(function(){
      
      var windowHeight = $(window).height();
      var detailsBannerHeight = windowHeight - ( windowHeight*20/100 );
      var galleryBannerHeight = windowHeight - ( windowHeight*75/100 );

      $('.lux-property-banner').css("height", detailsBannerHeight+"px")
	
      $('.lux-property-tabs .nav-pills li:nth-child(1), .lux-property-tabs .nav-pills li.dt').click(function(){
        $('.lux-property-banner').css("height", detailsBannerHeight+"px")
      });
      $('.lux-property-tabs .nav-pills li:nth-child(2), .lux-property-tabs .nav-pills li.gl').click(function(){
        $('.lux-property-banner').css("height", galleryBannerHeight+"px");
      });

      

      /* Display the property Details */
      $(".prop-description a").click(function(e){
        console.log("dfdfdf");
        e.preventDefault();
      });


    });
  }

  /* Get Image Path By ID */
  getImagePath(id){
    this.propertyService.getPropertyImagesById(id)
      .subscribe(images => {
        this.images = images;
        this.bannerImage = images[0]['image'];
        this.bannerImage = this.sanitizer.bypassSecurityTrustStyle(`url(${images[0]['image']})`);
      })
  }

  /* Get Facilities By ID */
  getFacilitiesById(id){
    this.propertyService.getFacilitiesById(id)
      .subscribe(facilities => {
        this.facilities_array = facilities;
        this.facilities = this.getFacilitiesField(facilities);
        
      })
  }

  getFacilitiesField(facilities){
    var temp = "";
    for(var i = 0; i < facilities.length; i++){
      temp = temp + facilities[i]['facility'] + ";  ";
    }
    return temp;
  }
  /* Get Property detail By ID */
  getPropertyById(id){
    this.propertyService.getPropertyById(id)
      .subscribe(property => {
        this.property = property[0];
        this.latitude = Number(property[0].latitude);
        this.longitude = Number(property[0].longitude);

        var description = this.property.web_remarks;
        
        $("#desc-content").html(description.substr(0, 800)+ "...  " + "<p class='show-more' data-toggle='collapse' data-target='#more-detail'>Show More</p>");
       
        $(".show-more").prev().css({"display": 'inline-block'});
        $("#more-detail").html(description.substr(801, description.length - 800));
      })
  }


  slice(properties){
    this.allProperties = properties;
    this.properties = properties.slice(0,9 * this.depth);
    
    var temp = [];
    for(var i = 0; i< properties.length; i++){
      if(properties[i].exclusive == "1")
        temp.push(properties[i]);
    }
    this.exclusives = temp.slice(0,2);
  }

  getPropertiesByTypeAndCategory(type, category){
    this.propertyService.getPropertiesByTypeAndCategory(type, category)
      .subscribe(properties => {
        this.slice(properties);
        
      })
  }
  getPropertiesByType(type){
    this.propertyService.getPropertiesByType(type)
      .subscribe(properties => {
        this.slice(properties);
        
      })
  }

  getProperties(){
    this.propertyService.getProperties()
      .subscribe(properties => {
        this.slice(properties);
      })
  }

  addProperties(){
    this.depth = this.depth + 1;
    this.properties = this.allProperties.slice(0, 9 * this.depth);
  }
  ngAfterViewInit(){
    $(document).delegate('*[data-toggle="lightbox"]', 'click', function(event) {
      event.preventDefault();
      return $(this).ekkoLightbox({
        always_show_close: true
      });
    });
    
  }

}
