import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgbCarouselModule, NgbAlertModule } from '@ng-bootstrap/ng-bootstrap';

/* Import Components */
import { ListComponent } from './list.component';
import { PropertyComponent } from '../components/property/property.component';
import { EnquireComponent } from '../components/enquire/enquire.component';
import { ShareComponent } from '../components/share/share.component';
import { EbrochureComponent } from '../components/ebrochure/ebrochure.component';

/* Import Filters*/
import { PropertyService } from '../services/property/property.service';
import { TitlePipe} from '../../filters/title/title.pipe';
import { PricePipeModule } from '../../filters/price/price.module';
import { PhonePipe} from '../../filters/phone/phone.pipe';
import { ParseHtmlPipe} from '../../filters/parsehtml/parsehtml.pipe';
import { AgentImagePipe} from '../../filters/agentimage/agentimage.pipe';

/* Import Module */
import { ListRoutingModule } from './list-routing.module';
import { AgmCoreModule} from "@agm/core";
import { ImagegalleryComponent } from '../components/imagegallery/imagegallery.component';


@NgModule({
    imports: [
        CommonModule,
        HttpClientModule,
        NgbCarouselModule.forRoot(),
        NgbAlertModule.forRoot(),
        ListRoutingModule,
        PricePipeModule,
        AgmCoreModule.forRoot({
            apiKey: "AIzaSyBeI4GDOgCIEOcJe6xt8BfY7k5BQStgsuQ"
        })
    ],
    declarations: [
        ListComponent,
        PropertyComponent,
        TitlePipe,
        PhonePipe,
        EnquireComponent,
        ShareComponent,
        ImagegalleryComponent,
        EbrochureComponent,
        ParseHtmlPipe,
        AgentImagePipe
    ],
    providers: [PropertyService],
    bootstrap: [ListComponent]
})
export class ListModule {}
