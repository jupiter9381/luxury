import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from './layout.component';

const routes: Routes = [
    {
        path: '',
        component: LayoutComponent,
        children: [
            { path: '', redirectTo: 'home', pathMatch:'full' },
            { path: 'home', loadChildren: './home/home.module#HomeModule' },
            { path: 'communities', loadChildren: './community/community.module#CommunityModule'},
            { path: 'communities/:title', loadChildren: './community/communitydetail/communitydetail.module#CommunitydetailModule'},
            { path: ':type', loadChildren: './list/list.module#ListModule' },
            { path: ':type/:emirate/:community/:name/:id', loadChildren: './list/list.module#ListModule' },
            
            { path: 'search', loadChildren: './list/list.module#ListModule' },
        ]
    }
];


@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})

export class LayoutRoutingModule {}
