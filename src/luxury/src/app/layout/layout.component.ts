import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.css']
})
export class LayoutComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  ngAfterViewInit(){
    $(document).ready(function(){
      $('.search-icon').click(function(e) {
        e.preventDefault();
        $('.search-container').delay(100).slideToggle(500);
        $('.top_search').delay(100).slideToggle(500);
        $('.drag').delay(100).slideToggle(500);
      });
    });
  }
}
