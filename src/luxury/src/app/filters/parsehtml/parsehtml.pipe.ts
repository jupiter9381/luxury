import {Pipe, PipeTransform} from '@angular/core';
import * as $ from 'jquery';
import { HtmlParser } from '@angular/compiler';

@Pipe({
    name: 'parseHtml'
})
export class ParseHtmlPipe implements PipeTransform {
    transform(value: string): object{
        //value = value.replace(/\s/g, "-");
        var html = $.parseHTML(value);
        console.log(html);
        return html;
    }
}