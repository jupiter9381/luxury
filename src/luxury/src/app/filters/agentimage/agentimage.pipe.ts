import {Pipe, PipeTransform} from '@angular/core';
import * as $ from 'jquery';
import { HtmlParser } from '@angular/compiler';

@Pipe({
    name: 'agentImage'
})
export class AgentImagePipe implements PipeTransform {
    transform(value: string): string{
        if(value != null)
            return "assets/images/agent/"+ value.replace(" ", "-") + ".jpg";
    }
}