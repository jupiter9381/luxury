import {Pipe, PipeTransform, NgModule} from '@angular/core';

@Pipe({
    name: 'transformPrice'
})

export class PricePipe implements PipeTransform {
    transform(value: number): string{
        var nf = Intl.NumberFormat();
        return nf.format(value);
    }
}