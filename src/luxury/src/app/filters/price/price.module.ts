import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';

import {PricePipe} from './price.pipe';

@NgModule({
    imports:[],
    declarations: [PricePipe],
    exports: [PricePipe]
})
export class PricePipeModule {}
