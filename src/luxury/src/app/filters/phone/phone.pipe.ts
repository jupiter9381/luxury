import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
    name: 'phoneFormat'
})
export class PhonePipe implements PipeTransform {
    transform(value: string): string{
        if(value != null){
            //var string = value.replace(/^1/,"+971");
            var temp = value.substr(1, value.length - 1);
            
            temp = "+971 " + temp.substr(0, 2) + " " + temp.substr(2, 3) + " " + temp.substr(5, 4);
            return temp;
        }
        
    }
}